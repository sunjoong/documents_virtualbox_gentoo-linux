;;; init.el --- Emacs init file

;; Copyright (C) 2012 Sunjoong Lee.

;; Author: Sunjoong Lee <sunjoong at gmail.com>

;;; Code:

(custom-set-variables
 ;; Editing::Indent
 '(indent-tabs-mode nil)                ; Indent Tabs Mode
 ;; Editing::Matching::Bookmark
 '(bookmark-default-file                ; Bookmark Default File
   (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
           "/.emacs.d/Editing/bookmarks"))
 ;; Editing::Editing Basics
 '(delete-selection-mode t)             ; Delete Selection Mode
 ;; Environment::Initialization
 '(inhibit-startup-message t)           ; Inhibit Startup Screen
 ;; Environment::I18n::Mule
 '(current-language-environment         ; Current Language Environment
   "Korean")
 ;; Environment::Mode Line
 '(column-number-mode t)                ; Column Number Mode
 ;; Environment::Mode Line::Display Time
 '(display-time-24hr-format t)          ; Display Time 24hr Format
 '(display-time-mode t)                 ; Display Time Mode
 ;; Environment::Data::Remember
 '(remember-mode-hook                   ; Remember Mode Hook
   '(org-remember-apply-template))
 '(remember-data-file                   ; Remember Data File
   (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
           "/.emacs.d/Applications/Org/notes.org"))
 ;; Applications::Calendar::Org
 '(org-id-locations-file                ; Org Id Locations File
   (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
           "/.emacs.d/Applications/Org/org-id-locations"))
 ;; Applications::Calendar::Org::Org Agenda
 '(org-agenda-files                     ; Org Agenda Files
   (list (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
                 "/.emacs.d/Applications/Org/todo.org")))
 ;; Applications::The Emacs shell
 '(eshell-directory-name                ; Eshell Directory Name
   (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
           "/.emacs.d/Applications/Eshell/"))
 ;; Files::Backup
 '(make-backup-files nil)               ; Make Backup Files
 ;; Files::Auto Save
 '(auto-save-default nil)               ; Auto Save Default
 '(auto-save-list-file-prefix           ; Auto Save List File Prefix
   (concat (replace-regexp-in-string "\\\\" "/" (getenv "HOME"))
           "/.emacs.d/Files/auto-save-list/saves-")))
;; custom-set-variables statement ends here.

(prefer-coding-system 'utf-8-unix)

;;; Local emacs variables
;; Local variables:
;; coding: utf-8-unix
;; mode: Emacs-Lisp
;; End:

;;; init.el file ends here.
